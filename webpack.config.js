'use strict';

const srcDir = '/frontend/src/';
const distDir = '/frontend/dist/';

const points = ['/frontend/react/index.jsx'];

const proxy = true;
const url = 'http://webpack.loc';
const port = '9000';
const hostname = (new URL(url)).hostname;
const openTarget = url.replace(hostname, `${hostname}:${port}`);

const htmlPages = ['index'];

const params = require('.' + srcDir + 'js/config.json');

const path = require('path');
const Webpack = require('webpack');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const plugins = [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin(),
    new CopyWebpackPlugin({patterns: [{from: '.' + srcDir + 'static'}]}),
];

if (!proxy) {
    const HtmlWebpackPlugin = require('html-webpack-plugin');
    htmlPages.map(page => {
        plugins.push(new HtmlWebpackPlugin({
            filename: page + '.html',
            template: __dirname + srcDir + 'html/' + page + '.html'
        }));
    })
}

module.exports = (env, options) => {

    const production = (typeof options.mode !== 'undefined' && options.mode === 'production');
    const rules = [
        {
            test: /\.m?jsx?$/,
            exclude: /(node_modules)/,
            use: {
                loader: 'babel-loader',
                options: {
                    "presets": [
                        ["@babel/preset-env",{
                             "targets": {"browsers": ["last 2 versions"]}
                        }],
                        ["@babel/preset-react", {"runtime": "automatic"}]
                    ]
                }
            }
        },
        {
            test: /\.scss$/,
            use: [
                MiniCssExtractPlugin.loader,
                {
                    loader: 'css-loader',
                    options: {
                        sourceMap: !production,
                        esModule: false
                    },
                },
                {
                    loader: 'postcss-loader',
                    options: {
                        postcssOptions: {
                            plugins: [
                                require('autoprefixer')({
                                    overrideBrowserslist: ['> 2%']
                                })
                            ],
                            sourceMap: !production,
                        }
                    }
                },
                {
                    loader: 'sass-loader',
                    options: {sourceMap: !production},
                },
                {
                    loader: "@epegzz/sass-vars-loader",
                    options: {
                        syntax: 'scss',
                        vars: params
                    }
                }
            ],
        },
        {
            test: /\.css$/,
            use: [
                MiniCssExtractPlugin.loader,
                'css-loader',
                {
                    loader: 'postcss-loader',
                    options: {
                        postcssOptions: {
                            plugins: [
                                require('cssnano')({
                                    preset: [
                                        'default',
                                        {discardComments: {removeAll: true}}
                                    ],
                                }),
                            ],
                        }
                    }
                }
            ],
        },
        {
            test: /\.(eot|ttf|woff|woff2|svg)$/i,
            exclude: /(images)/,
            use: [
                {
                    loader: 'file-loader',
                    options: {
                        name: '[name][hash].[ext]'
                    }
                }
            ]
        },
        {
            test: /\.(cur|gif|png|jpe?g|svg)$/i,
            exclude: /(fonts)/,
            use: [
                {
                    loader: 'url-loader',
                    options: {
                        limit: 1 * 1024,
                        name: '[name].[ext]',
                    }
                }
            ]
        }
    ];


    return {

        mode: typeof production === 'undefined' ? 'development' : options.mode,

        entry: (() => {
            let entry = {};
            points.map((item) => {
                    let path;
                    let name;
                if(item.startsWith('\/')) {
                    name = (item.substring(item.lastIndexOf('/') + 1)).split(".")[0];
                    path = '.' + item;
                } else {
                    name = item.replace(new RegExp('/', 'g'), '-');
                    path = '.' + srcDir + item + '.js';
                }
                entry[name] = path;
            });

            return entry;
        })(),

        output: {
            path: __dirname + distDir,
            filename: '[name].js',
        },

        optimization: {
            /*splitChunks: {chunks: 'all'},*/
            minimize: production
        },

        devtool: production ? false : 'source-map',

        watchOptions: {
            aggregateTimeout: 100
        },

        plugins: plugins,

        devServer: {
            host: hostname,
            port: port,
            proxy: (proxy) ? {'*': {target: url}} : {},
            open: {
                target: [openTarget],
                app: {
                    name: 'google-chrome',
                    //arguments: ['--incognito', '--new-window'],
                },
            },                     
            watchFiles: ['*.php'],
            static: {
                directory: __dirname + distDir,
                watch: true,
                serveIndex: true
            },
            client: {
                reconnect: true,
            },
            liveReload: true,
            hot: false,
            devMiddleware: {
                writeToDisk: true
            }
        },

        module: {
            rules: rules
        },

        resolve: {
            extensions: ['.js', '.jsx', '.ts', '.tsx', '.json']
        },
    }
}