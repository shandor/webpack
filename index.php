<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <title><?php echo 'Complete React Webpack Configuration Title' ?></title>
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <link href="/frontend/dist/index.css" rel="stylesheet" type="text/css">
</head>

<body>
  <?php echo 'React Webpack App' ?>
  <div id="root"></div>
  <script src="/frontend/dist/index.js" async defer></script>
</body>

</html>